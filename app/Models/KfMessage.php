<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KfMessage extends Model
{
    protected $table = 'yy_kf_message';

    protected $fillable = ['dialog_id', 'role', 'content', 'status', 'is_auto'];

    public function dialog()
    {
        return $this->belongsTo(KfDialog::class, 'dialog_id');
    }
}