<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OftenUsedWords extends Model
{
    protected $table = 'yy_kf_often_used_words';

    protected $fillable = ['server_id', 'platform', 'content'];

}