<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookList extends Model
{
    protected $table = 'yy_kf_book_list';

    protected $fillable = [
        'server_id', 'platform', 'name', 'header_text', 'list', 'footer_text', 'redirect_to'
    ];
}