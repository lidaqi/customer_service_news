<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KfDialog extends Model
{
    protected $table = 'yy_kf_dialog';

    protected $fillable = ['appid', 'user_id', 'channel', 'server_id', 'platform', 'status', 'admin_id', 'nickname', 'avatar'];

    public function messages()
    {
        return $this->hasMany(KfMessage::class, 'dialog_id');
    }
}