<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'yy_cps_book';

    public function scopeNormal($query)
    {
        return $query->where('status', 0)->where('is_ground', 1)->where('is_hot', 0);
    }
}
